package Compute.Class;

import Common.Task;

import java.io.Serializable;

interface Operation extends Task<Boolean>, Serializable {
}
