package Compute.Class;

import Compute.Class.Operation;

import java.math.BigInteger;

public class IsPrimeTask implements Operation {

    private int entier;

    public IsPrimeTask(int entier) {
        this.entier = entier;
    }

    @Override
    public Boolean execute() {
        return this.isPrime();
    }

    private boolean isPrime() {
        if (this.entier % 2 == 0 || this.entier == 1) {
            return false;
        } else {
            boolean estPremier = true;
            for (int i = 3; i < this.entier / 2; i += 2) {
                if (this.entier % i == 0) {
                    estPremier = false;
                }
            }
            return estPremier;
        }
    }
}

