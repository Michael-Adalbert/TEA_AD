package Client;

import Common.Compute;
import Compute.Class.IsPrimeTask;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.sql.*;

public class MainServer {

    private static Connection c = null;

    public static void main(String[] args) {

        try {


            if (args.length < 5) {
                System.out.println("Vous devez definir les parametre de la maniere suivante :");
                System.out.println("urlServeur={<adress>:<port>;<adress>:<port>;...}");
                System.out.println("dbName=<nomDeLaBase>");
                System.out.println("urlBase=<adress>:<port>");
                System.out.println("dbUserName=<username>");
                System.out.println("dbUserPassWord=<password>");
                System.exit(1);
            }

            if (System.getSecurityManager() == null) {
                System.setSecurityManager(new RMISecurityManager());
            }

            String[] url = {"127.0.0.1:1099"};
            String dbName = "", urlBase = "127.0.0.1:5432", userName = "postgres", passWord = "postgres";
            //Traitement des arguments
            for (String argument : args) {
                String[] argumentData = argument.split("=");
                String key = argumentData[0];
                String data = argumentData[1];
                if ("urlServeur".equals(key)) {
                    url = data.replace("{", "").replace("}", "").split(";");
                } else if ("dbName".equals(key)) {
                    dbName = data;
                } else if ("urlBase".equals(key)) {
                    urlBase = data;
                } else if ("dbUserName".equals(key)) {
                    userName = data;
                } else if ("dbUserPassWord".equals(key)) {
                    passWord = data;
                }
            }

            Class.forName("org.postgresql.Driver");
            urlBase = "jdbc:postgresql://" + urlBase + "/" + dbName;
            System.out.println(urlBase);
            c = DriverManager.getConnection(urlBase, "postgres", "postgres");
            initBase();


            for (String u : url) {
                String[] urlDecomposer = u.split(":");
                String adress = urlDecomposer[0];
                int port = Integer.parseInt(urlDecomposer[1]);
                ServerHandler s = new ServerHandler(adress, port);
                s.start();
            }

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    private static void initBase() {
        try {
            Statement stmt = c.createStatement();
            String sql;


            sql = "DROP TABLE IF EXISTS isPrime;";
            stmt.executeUpdate(sql);

            sql = "CREATE TABLE isPrime (number INTEGER, estPremier BOOLEAN);";
            stmt.executeUpdate(sql);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    static void insertRes(int n, boolean res) {

        try {
            Statement stmt = c.createStatement();
            String sql;


            sql = String.format("INSERT INTO isPrime  VALUES (%d,%s)", n, res);
            stmt.executeUpdate(sql);

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}

class ServerHandler extends Thread {

    private static Compteur cpt = new Compteur();
    private int port;
    private String adress;

    ServerHandler(String adress, int port) {
        this.adress = adress;
        this.port = port;
    }

    @Override
    public void run() {
        try {
            String name = "rmi://" + adress + ":" + this.port + "/ComputePrime";
            Compute c = (Compute) Naming.lookup(name);
            while (true) {
                int i = cpt.getNextValue();
                IsPrimeTask primeTask = new IsPrimeTask(i);
                boolean b = (boolean) c.executeTask(primeTask);
                MainServer.insertRes(i, b);
                Thread.sleep(20);
            }
        } catch (RemoteException | NotBoundException | MalformedURLException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}

class Compteur {
    private volatile int cpt = 0;

    synchronized int getNextValue() {
        this.cpt++;
        return this.cpt;
    }

}
