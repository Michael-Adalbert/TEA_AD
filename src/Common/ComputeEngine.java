package Common;

import Common.Compute;
import Common.Task;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class ComputeEngine extends UnicastRemoteObject implements Compute {

    public ComputeEngine() throws RemoteException {
    }

    @Override
    public Object executeTask(Task t) {
        System.out.println("--- executeTask ");
        return t.execute();
    }
}
