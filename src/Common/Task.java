package Common;

public interface Task<T> {
    T execute();
}
