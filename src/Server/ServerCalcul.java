package Server;

import Common.Compute;
import Common.ComputeEngine;


import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;


public class ServerCalcul {
    public static void main(String[] args) {
        int port = 1099;
        String adress = "127.0.0.1";

        for (String argument : args) {
            String[] argumentData = argument.split("=");
            String key = argumentData[0];
            String value = argumentData[1];

            if ("adress".equals(key)) {
                adress = value;
            } else if ("port".equals(key)) {
                port = Integer.parseInt(value);
            }
        }


        try {
            LocateRegistry.createRegistry(port);
            Compute engine = new ComputeEngine();
            String url = "rmi://" + adress + ":" + port + "/ComputePrime";
            System.out.println("URL => " + url);
            Naming.rebind(url, engine);
        } catch (RemoteException | MalformedURLException e) {
            e.printStackTrace();
        }
    }
}


